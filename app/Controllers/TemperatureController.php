<?php

declare(strict_types=1);

namespace App\Controllers;

use App\Models\Sms;
use App\Models\Weather;
use App\Helpers\Route;

Class TemperatureController {
	public function index()
    {
    	$weather = new Weather();
    	$sms = new Sms();
    	$smsToken = $sms->getToken();
    	$smsToken = $smsToken['status'] == 200 ? $smsToken['data']['access_token'] : null ;
    	$number = '+306978745957';
    	
        $weatherData =  $weather->getWeather('http://api.openweathermap.org/data/2.5/weather?q=thessaloniki&appid=b385aa7d4e568152288b3c9f5c2458a5&units=metric');
  
        if($weatherData['status'] == 200)
        {
        	if($weatherData['data']['main']['temp']>20)
        	{	if($smsToken)
        			$sms->sentSms($smsToken,$number, "Hi User, Your Temperature more than 20C. <".$weatherData['data']['main']['temp'].">");
        		return Route::json_response(['status' => 200 , 'data' => 'Lower temperature'] , 200);

        	}
        	else{
        		if($smsToken)
        			$sms->sentSms($smsToken,$number, "Hi User, Your Temperature less than 20C. <".$weatherData['data']['main']['temp'].">");
        		return Route::json_response(['status' => 200 , 'data' => 'Higher temperature'] , 200);
        	}
        } else
        {
        	return Route::json_response(['status' => 500 , 'data' => 'error'] , 500);
        }


    }

}

?>