<?php

declare(strict_types=1);

namespace App\Helpers;

interface ApiInterface
{
	public  function setValues($url,$method,$header,$body);
	public  function callExternalAPi();
} 
  
?>