<?php

declare(strict_types=1);

namespace App\Helpers;

Class Api implements ApiInterface
{
	private $url;
	private $method;
	private $header;
	private $body;

	public  function setValues($url,$method,$header,$body): void
	{
		$this->url = $url;
		$this->method = $method;
		$this->header = $header;
		$this->body = $body;
	}
	public  function callExternalAPi()
	{
		
		$curl = curl_init();

		$request = array(
		  CURLOPT_URL => $this->url,
		  CURLOPT_RETURNTRANSFER => true,
		  CURLOPT_ENCODING => '',
		  CURLOPT_MAXREDIRS => 10,
		  CURLOPT_TIMEOUT => 0,
		  CURLOPT_FOLLOWLOCATION => true,
		  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		  CURLOPT_CUSTOMREQUEST => $this->method,
		  CURLOPT_HTTPHEADER => $this->header,
		  CURLOPT_POSTFIELDS => $this->body
		);

		curl_setopt_array($curl, $request);

		$response = curl_exec($curl);
		$err = curl_error($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);

		if ($err) {
		  return ['status' => 500 , 'data' => $err];
		} else {
		  if ( $code == 200)
		  {
		  	return ['status' => $code , 'data' => json_decode($response,true)];
		  } else 
		  {
			return ['status' => $code , 'data' => $response];
		  }
		}
		
		

	}

	public function getApiCallResult($url,$method,$header,$body)
	{
		self::setValues($url,$method,$header,$body);
		return $this->callExternalAPi();
	}

}

?>