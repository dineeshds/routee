<?php

declare(strict_types=1);

namespace App\Models;
use App\Helpers\Api;

Class Weather 
{
	public function getWeather($url)
	{
		$api = new Api();
		return $api->getApiCallResult($url,'GET',array("content-type: application/json"),[]);
	}

}

?>