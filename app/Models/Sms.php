<?php

declare(strict_types=1);

namespace App\Models;
use App\Helpers\Api;

Class Sms 
{
	public function getToken()
	{
		$api = new Api();
		return $api->getApiCallResult('https://auth.routee.net/oauth/token','POST',array("authorization: Basic NWY5MTM4Mjg4YjcxZGUzNjE3YTg3Y2QzOkNLd0c4S1h0aU8=","content-type: application/x-www-form-urlencoded"),"grant_type=client_credentials"); 
	}
	public function sentSms($token, $number, $message)
    {
        $api = new Api();
        $body = "{ \"body\": \"$message\",\"to\" : \"$number\",\"from\": \"amdTelecom\",\"callback\": { \"strategy\": \"OnCompletion\", \"url\": \"http://www.yourserver.com/message\"}}";
        $header = array(
				    "authorization: Bearer ".$token,
				    "content-type: application/json"
				  );
		return $api->getApiCallResult('https://connect.routee.net/sms','POST',$header,$body); 
    }

}

?>